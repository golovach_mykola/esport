<x-app-layout>
    <x-slot name="header">
        <div style="display: flex">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight inline mt-2">
                Players
            </h2>


            <div style="margin-left: auto; margin-right: 0;" class="inline">
                <form method="get" action="/dashboard/?">
                   <input type="hidden" value="{{$team_id}}" name="team_id">
                   <input type="hidden" value="{{$sortType}}" name="sortType">
                    <input type="search" placeholder="{{$search}}" aria-label="Search" name="search_input" id="q" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2" >
                    <button style="background-color: rgba(104,117,245,255)" type="submit" class="inline-flex justify-center rounded-md border border-transparent py-2 px-4 text-sm font-medium text-white shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Search</button>

                </form>
            </div>

            <div style="margin-left: auto; margin-right: 0;" class="inline">
                <form method="get">
                <select name="teams" onchange="window.location.href=this.options[this.selectedIndex].value" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2">
                    <option value="/dashboard?team=0" selected >Team</option>
                    @foreach($teams as $team)
                        <option value="/dashboard?team_id={{$team->id}}&search_input={{$search}}">{{$team->name}}</option>
                    @endforeach
                </select>
                </form>
            </div>
            &nbsp;
            <form action="{{route('dashboard')}}" class="bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <button type="submit">
                    clear all filters
                </button>
            </form>

        </div>
    </x-slot>
    <br>
    <div style="width:80%;  margin: auto;" >
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg "  >
            <table id="myTable" class="w-full text-sm text-left text-gray-500 dark:text-gray-400 dataTable">
                <thead class="text-xs text-gray-700 uppercase bg-white">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        <?php
                        if ($sortType === 'Asc')
                            $sortType = 'Desc';
                        else
                            $sortType = 'Asc';
                        ?>
                        <a href="/dashboard?sortBy=nickname&sortType={{$sortType}}&search_input={{$search}}&team_id={{$team_id}}">Player</a>
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Team
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Maps
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Rounds
                    </th>
                    <th scope="col" class="px-6 py-3">
                        K-D Diff
                    </th>
                    <th scope="col" class="px-6 py-3">
                        K/D
                    </th>
                    <th scope="col" class="px-6 py-3">
                        <a href="/dashboard?sortBy=rating_1_0&sortType={{$sortType}}&search_input={{$search}}&team_id={{$team_id}}">Rating(1.0)</a>
                    </th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($players as $player)
                    <tr class="bg-white ">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap bg-white">
                            {{$player->nickname}}
                        </th>
                        <td class="px-6 py-4">
                            {{$player->team->name}}
                        </td>
                        <td class="px-6 py-4">
                            {{$player->maps_played}}
                        </td>
                        <td class="px-6 py-4">
                            {{$player->rounds_played}}
                        </td>
                        <td class="px-6 py-4">
                            {{$player->kills_on_deaths_different}}
                        </td>
                        <td class="px-6 py-4">
                            {{$player->kills_on_deaths}}
                        </td>
                        <td class="px-6 py-4">
                            {{$player->rating_1_0}}
                        </td>
                        <td class="px-6 py-4">
                            <a href="{{route('matches', ['id'=>$player->id])}}"
                               class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Match statistics</a>
                        </td>
                        <td class="px-6 py-4">
                            <a href="{{ route('player',['id'=>$player->id]) }}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Detail</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <div class="d-flex justify-content-center">
            {{ $players->links()   }}
        </div>
        <div class="mt-1 text-gray-400">
            <x-last-update />
        </div>
    </div>
    <br>
</x-app-layout>

