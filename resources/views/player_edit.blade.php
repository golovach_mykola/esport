<x-app-layout>
    <div class="container mx-auto max-w-4xl mt-3 pb-14">
        <a href="{{ route('player',['id'=>$player->id]) }}"
           class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-left">Back to
            player</a>
    </div>
    <div class="container max-w-4xl mx-auto">
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <div class="alert alert-success" role="alert">{{\Illuminate\Support\Facades\Session::get('message')}}</div>
        @endif
        <form action="{{ route('update.player',['id'=>$player->id])}}" method="post">
            @csrf
            @method('PUT')
            <div class="container max-w-4xl mx-auto columns-2 gap-2 mt-2">
                <div class="container relative bg-white rounded-lg">
                    <div class="mb-3 pt-3 mx-3">
                        <label for="nickname" class="form-label ">Nickname</label>
                        <input type="text" name="nickname"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter player nickname" value="{{$player->nickname}}"/>
                        @error('nickname')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="team_id" class="form-label">Team</label>
                        <select
                            class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                            name="team_id">
                            <option value="">Select Team</option>
                            @foreach($teams as $t)
                                @if($player->team_id==$t->id)
                                    <option selected value="{{$t->id}}">{{$t->name}}</option>
                                @else
                                    <option value="{{$t->id}}">{{$t->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('team_id')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="link" class="form-label">HLTV player link</label>
                        <input type="text" name="link"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter HLTV player link" value="{{$player->link}}"/>
                        @error('link')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="image" class="form-label">Image link</label>
                        <input type="text" name="image"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter image link" value="{{$player->image}}"/>
                        @error('image')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="maps_played" class="form-label">Maps played</label>
                        <input type="text" name="maps_played"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter maps played" value="{{$player->maps_played}}"/>
                        @error('maps_played')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="rounds_played" class="form-label">Rounds played</label>
                        <input type="text" name="rounds_played"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter rounds played" value="{{$player->rounds_played}}"/>
                        @error('rounds_played')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="rating_1_0" class="form-label">Rating 1.0</label>
                        <input type="text" name="rating_1_0"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter Rating 1.0" value="{{$player->rating_1_0}}"/>
                        @error('rating_1_0')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="kills_on_deaths_different" class="form-label">kills_on_deaths_different</label>
                        <input type="text" name="kills_on_deaths_different"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="Enter image link" value="{{$player->kills_on_deaths_different}}"/>
                        @error('kills_on_deaths_different')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="kills_on_deaths" class="form-label">kills_on_deaths</label>
                        <input type="text" name="kills_on_deaths"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="kills_on_deaths" value="{{$player->kills_on_deaths}}"/>
                        @error('kills_on_deaths')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3 pb-3">
                        <label for="rating_2_0" class="form-label">Rating 2.0</label>
                        <input type="text" name="rating_2_0"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="rating_2_0" value="{{$player->rating_2_0}}"/>
                        @error('rating_2_0')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                </div>
                <div class="container relative bg-white rounded-lg">
                    <div class="my-3 mx-3 pt-3">
                        <label for="kast" class="form-label">KAST</label>
                        <input type="text" name="kast"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="kast" value="{{$player->kast}}"/>
                        @error('kast')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="impact" class="form-label">Impact</label>
                        <input type="text" name="impact"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="impact" value="{{$player->impact}}"/>
                        @error('impact')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="damage_per_round_av" class="form-label">Average damage/Round</label>
                        <input type="text" name="damage_per_round_av"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="damage_per_round_av" value="{{$player->damage_per_round_av}}"/>
                        @error('damage_per_round_av')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="total_kills" class="form-label">Total kills</label>
                        <input type="text" name="total_kills"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="total_kills" value="{{$player->total_kills}}"/>
                        @error('total_kills')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="total_deaths" class="form-label">Total deaths</label>
                        <input type="text" name="total_deaths"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="total_deaths" value="{{$player->total_deaths}}"/>
                        @error('total_deaths')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="headshots" class="form-label">Headshots %</label>
                        <input type="text" name="headshots"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="headshots" value="{{$player->headshots}}"/>
                        @error('headshots')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="grenade_damage_round" class="form-label">Grenade damage/Round</label>
                        <input type="text" name="grenade_damage_round"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="grenade_damage_round" value="{{$player->grenade_damage_round}}"/>
                        @error('grenade_damage_round')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="assists_per_round" class="form-label">Assists/Round</label>
                        <input type="text" name="assists_per_round"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="assists_per_round" value="{{$player->assists_per_round}}"/>
                        @error('assists_per_round')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 mx-3">
                        <label for="saved_by_teammate_per_round" class="form-label">Saved by teammate/Round</label>
                        <input type="text" name="saved_by_teammate_per_round"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="saved_by_teammate_per_round"
                               value="{{$player->saved_by_teammate_per_round}}"/>
                        @error('saved_by_teammate_per_round')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="my-3 pb-3 mx-3">
                        <label for="saved_teammates_per_round" class="form-label">Saved teammates/Round</label>
                        <input type="text" name="saved_teammates_per_round"
                               class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2"
                               placeholder="saved_teammates_per_round" value="{{$player->saved_teammates_per_round}}"/>
                        @error('saved_teammates_per_round')
                        <p class="text-danger text-red-600">{{$message}}</p>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit"
                    class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 mr-3" value="">
                Update player
            </button>
        </form>
    </div>
</x-app-layout>
