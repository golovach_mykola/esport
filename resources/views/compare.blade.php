<x-app-layout>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    <div class="container max-w-4xl mx-auto mt-3 columns-2 gap-3">
        @if($pl1)
            <div class="container bg-white relative overflow-x-auto rounded-lg shadow-md">
                <table class="w-full text-left text-sm text-gray-500">
                    <tr>
                        <td rowspan="2" class="bg-white px-4 py-3 text-xs uppercase text-gray-700"><img class="border-2 rounded-lg" style="max-height: 100px" src="{{$player1->image}}"></td>
                        <td style="height: 30px"><a class="float-right mr-4 mt-3" href="{{route('compare.player',array('pl2'=>$pl2))}}">remove player</a></td>
                    </tr>
                    <tr>
                        <td class="text-2xl text-black">{{$player1->nickname}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Rating 2.0</th>
                        <td>{{$player1->rating_2_0}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">KPR</th>
                        <td>{{round($player1->total_kills/$player1->rounds_played,2)}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Assists / Round</th>
                        <td>{{$player1->assists_per_round}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">KAST</th>
                        <td>{{$player1->kast}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Impact</th>
                        <td>{{$player1->impact}}</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Headshots %</th>
                        <td>{{$player1->headshots}}%</td>
                    </tr>
                    <tr>
                        <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Impact</th>
                        <td>{{$player1->impact}}</td>
                    </tr>
                </table>
            </div>
        @else
            <div class="container columns-1">
                <div class="container relative overflow-x-auto rounded-lg shadow-md">
                    <table class="w-full text-left bg-white text-sm text-gray-500">
                        <tr>
                            <td rowspan="2" class="px-4 py-3" style="max-width: 86px"><img class="border-2 rounded-lg" style="max-height: 100px;" src="https://www.hltv.org/img/static/player/player_silhouette.png"></td>
                            <td class="text-2xl pt-4 font-bold">Player 1</td>
                        </tr>
                        <tr>
                            <td>
                                <form action='{{route('compare.player')}}' class="d-flex">
                                    <input class="mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2" type="search" placeholder="Search player..." aria-label="Search" name="pl1" id="pl1">
                                    <input type="hidden" name="pl2" value="{{$pl2}}"></input>
                                </form>
                            </td>
                        </tr>
                    </table>

                </div>
                <div id="player-list-one" class="container relative overflow-y-auto rounded-lg shadow-md bg-white" style="max-height: 600px">

                </div>
            </div>
        @endif
            @if($pl2)
                <div class="container relative overflow-x-auto rounded-lg shadow-md">
                    <table class="w-full bg-white text-left text-sm text-gray-500">
                        <tr>
                            <td rowspan="2" class="bg-white px-4 py-3 text-xs uppercase text-gray-700"><img class="border-2 rounded-lg" style="max-height: 100px" src="{{$player2->image}}"></td>
                            <td style="height: 30px"><a class="float-right mr-4 mt-3" href="{{route('compare.player',array('pl1'=>$pl1))}}">remove player</a></td>
                        </tr>
                        <tr>
                            <td class="text-2xl text-black">{{$player2->nickname}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Rating 2.0</th>
                            <td>{{$player2->rating_2_0}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">KPR</th>
                            <td>{{round($player2->total_kills/$player2->rounds_played,2)}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Assists / Round</th>
                            <td>{{$player2->assists_per_round}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">KAST</th>
                            <td>{{$player2->kast}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Impact</th>
                            <td>{{$player2->impact}}</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Headshots %</th>
                            <td>{{$player2->headshots}}%</td>
                        </tr>
                        <tr>
                            <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Impact</th>
                            <td>{{$player2->impact}}</td>
                        </tr>
                    </table>
                </div>
            @else
                <div class="container relative overflow-x-auto rounded-lg shadow-md">
                    <table class="w-full text-left bg-white text-sm text-gray-500">
                        <tr>
                            <td rowspan="2" class="px-4 py-3" style="max-width: 86px"><img class="border-2 rounded-lg" style="max-height: 100px;" src="https://www.hltv.org/img/static/player/player_silhouette.png"></td>
                            <td class="text-2xl pt-4 font-bold">Player 2</td>
                        </tr>
                        <tr>
                            <td>
                            <form action='{{route('compare.player')}}' class="d-flex">
                                <input type="hidden" name="pl1" value="{{$pl1}}"></input>
                                <input class="mb-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2" type="search" placeholder="Search player..." aria-label="Search" name="pl2" id="pl2">
                            </form>
                            </td>
                        </tr>
                    </table>
                    <div id="player-list-two" class="container relative overflow-y-auto rounded-lg shadow-md bg-white" style="max-height: 600px">

                    </div>
                </div>
            @endif
        <script type="application/javascript">
            const search1=document.getElementById('pl1');
            const search2=document.getElementById('pl2');
            const playerList1=document.getElementById('player-list-one');
            const playerList2=document.getElementById('player-list-two');
            const getPlay={{\Illuminate\Support\Js::from($players)}};
            const getTeam={{\Illuminate\Support\Js::from($teams)}};
            let pl1={{\Illuminate\Support\Js::from($pl1)}};
            let pl2={{\Illuminate\Support\Js::from($pl2)}};

            function findp(nickname,players){
                let playerss = players.filter(place => {
                    const regex = new RegExp(nickname, 'gi');
                    return place.nickname.match(regex);
                })
                if (nickname.length===0){
                    playerss = [];
                }
                return playerss;
            }
            function displayPlayersOne(){
                const playerArray = findp(this.value,getPlay);
                if (pl2==null)pl2 = "";
                const html = playerArray.map(place=>{
                    return `<a href="/compare?pl1=${place.id}&pl2=${pl2}">
                                <div class="container bg-gray-100 rounded-lg m-2" style="height: 80px; width: 410px" href="/compare?pl1=${place.id}&pl2=${pl2}">
                                    <img src="${place.image}" style="max-height:70px;" class="float-left bg-white rounded-lg mt-1 ml-1" href="/compare?pl1=${place.id}&pl2=${pl2}" lt="haha"/>
                                    <a href="/compare?pl1=${place.id}&pl2=${pl2}" class="text-3xl float-left mt-5 ml-3">${place.nickname} </a>
                                    <a href="/compare?pl1=${place.id}&pl2=${pl2}" class="text-xl float-right mt-2 mr-2">${getTeam[place.team_id-1].name}</a>
                                </div>
                            </a>`
                }).join('');
                playerList1.innerHTML=html;
            }
            function displayPlayersTwo(){
                const playerArray = findp(this.value,getPlay);
                if (pl1==null)pl1= "";
                const html = playerArray.map(place=>{
                    return `<a href="/compare?pl1=${pl1}&pl2=${place.id}">
                                <div class="container bg-gray-100 rounded-lg m-2" style="height: 80px; width: 410px" href="/compare?pl1=${pl1}&pl2=${place.id}">
                                    <img src="${place.image}" style="max-height:70px;" class="float-left bg-white rounded-lg mt-1 ml-1" href="/compare?pl1=${pl1}&pl2=${place.id}" lt="haha"/>
                                    <a href="/compare?pl1=${pl1}&pl2=${place.id}" class="text-3xl float-left mt-5 ml-3">${place.nickname} </a>
                                    <a href="/compare?pl1=${pl1}&pl2=${place.id}" class="text-xl float-right mt-2 mr-2">${getTeam[place.team_id-1].name}</a>
                                </div>
                            </a>`
                }).join('');
                playerList2.innerHTML=html;
            }
            if(search1!==null)search1.addEventListener('keyup',displayPlayersOne);
            if(search2!==null)search2.addEventListener('keyup',displayPlayersTwo);
        </script>
    </div>
</x-app-layout>
