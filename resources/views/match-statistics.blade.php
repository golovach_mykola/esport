<x-app-layout>
    <x-slot name="header" >

        <div class="flex">
            <div class="inline">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mt-2">
                    {{ __('Matches of player ')  }} {{$p_name->nickname}}
                </h2>
            </div>
            <div style="margin-left: auto; margin-right: 0;" class="inline ">
                <form method="get" action="/player/{{$id}}/matches">
                    <input type="hidden" value="{{$map_id}}" name="map_id">
                    <input type="hidden" value="{{$sortType}}" name="sortType">
                    <input type="search" placeholder="{{$search}}" aria-label="Search" name="search_input" id="q" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2" >
                    <button style="background-color: rgba(104,117,245,255)" type="submit" class="inline-flex justify-center rounded-md border border-transparent py-2 px-4 text-sm font-medium text-white shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Search by date</button>
                </form>
            </div>
            <div style="margin-left: auto; margin-right: 0;" class="">
                <form method="get">
                    <select name="maps" onchange="window.location.href=this.options[this.selectedIndex].value" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2">
                        <option value="/player/{{$id}}/matches?map=0" selected >Map</option>
                        @foreach($maps as $map)
                            <option value="/player/{{$id}}/matches?map_id={{$map->id}}&search_input={{$search}}">{{$map->name}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            &nbsp;
            <form action="{{route('matches',['id'=>$id])}}" class="bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <button type="submit">
                    clear all filters
                </button>
            </form>

        </div>
    </x-slot>

    <div style="width:80%;  margin: auto;" >
        <div class="container mx-auto mt-3 pb-14">
            <a href="{{ route('player',['id'=>$id]) }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-left">Player details</a>

            <a href="{{ route('dashboard') }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-right">Players dashboard</a>
        </div>

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg "  >
            <table id="myTable" class="w-full text-sm text-left text-gray-500 dark:text-gray-400 dataTable">
                <thead class="text-xs text-gray-700 uppercase ">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        <?php
                        if($sortType==='Asc')
                            $sortType='Desc';
                        else
                            $sortType='Asc';
                        ?>
                        <a href="/player/{{$id}}/matches?sortBy=date_time&sortType={{$sortType}}&search_input={{$search}}&map_id={{$map_id}}">Date</a>
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Team
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Opponent
                    </th>
                    <th scope="col" class="px-6 py-3">
                        K - D
                    </th>
                    <th scope="col" class="px-6 py-3">
                        + / -
                    </th>

                    <th scope="col" class="px-6 py-3">
                        <a href="/player/{{$id}}/matches?sortBy=rating&sortType={{$sortType}}&search_input={{$search}}&map_id={{$map_id}}">Rating</a>
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Map
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($matches as $match)
                    <tr class="bg-white ">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap bg-white">
                            {{$match->date_time}}
                        </th>
                        <td class="px-6 py-4">
                            {{$match->team->name}}
                        </td>
                        <td class="px-6 py-4">
                            {{$match->opponent_team->name}}
                        </td>
                        <td class="px-6 py-4">
                            {{$match->kills}} - {{$match->deaths}}
                        </td>
                        <td class="px-6 py-4">
                            @if(($match->kills - $match->deaths) == 0)
                                {{($match->kills - $match->deaths) }}
                            @elseif(($match->kills - $match->deaths)  < 0)
                                <p style="color: red">{{($match->kills - $match->deaths)}}</p>
                            @else
                                <p style="color: darkgreen">{{($match->kills - $match->deaths)}}</p>
                            @endif
                        </td>
                        <td class="px-6 py-4">
                            {{$match->rating}}
                        </td>
                        <td class="px-6 py-4">
                            {{$match->map->name}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <thead class="text-xs text-gray-700 uppercase ">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Team
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Opponent
                    </th>
                    <th scope="col" class="px-6 py-3">
                        K - D
                    </th>
                    <th scope="col" class="px-6 py-3">
                        + / -
                    </th>

                    <th scope="col" class="px-6 py-3">
                        Rating
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Map
                    </th>
                </tr>
                </thead>
            </table>
        </div>
        <br>
        <div class="d-flex justify-content-center">
            {{ $matches->links()   }}
        </div>
        <div class="mt-1 text-gray-400">
            <x-last-update />
        </div>
    </div>
    <br>
</x-app-layout>
