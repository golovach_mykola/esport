<x-app-layout>
    <x-slot name="header">
        <div class="">
            <div class="">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mt-2">
                    {{ __('Leaderboard')  }}
                </h2>
            </div>
        </div>
    </x-slot>
    <div class="container max-w-6xl mx-auto grid-cols-1 grid gap-2 sm:grid-cols-3">
        <div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">

                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        RATING 1.0
                    </h2>
                </div>

                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">Rating 1.0</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->rating_1_0 as $item)
                        <tr>
                            <th scope="row" class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white">
                                <a href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->rating_1_0}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">

                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        RATING 2.0
                    </h2>
                </div>

                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">Rating 2.0</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->rating_2_0 as $item)
                        <tr>
                            <th scope="row" class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white">
                                <a href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->rating_2_0}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        KD diff
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">KD_diff</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->KD_diff as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->kills_on_deaths_different}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Damage per round
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">DamagePR</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->DamagePR as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->damage_per_round_av}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Total Kills
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">KPR</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->Kills as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->total_kills}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Kills per round
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">Kpr</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->KillsPR as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->KPR}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Death Per Round
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">DeathPR</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->DeathPR  as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->DeathPR}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Asist Per Round
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700  bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">AsistPR</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->AsistPR as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->assists_per_round}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        KAST
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">KAST</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->kast as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->kast}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="container mx-auto relative overflow-x-auto rounded-lg shadow-md bg-white my-2">
                <div class="container px-4 mt-3">
                    <h2 class="font-semibold text-xl">
                        Headshots
                    </h2>
                </div>
                <table class="w-full bg-white text-left text-sm text-gray-500">
                    <thead class="text-xs text-gray-700 uppercase bg-white">
                    <tr>
                        <th scope="col" class="px-6 py-3 pt-4">Player</th>
                        <th scope="col" class="px-6 py-3">Team</th>
                        <th scope="col" class="px-6 py-3">Headshots</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($players->headshots as $item)
                        <tr>
                            <th scope="row"
                                class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white"><a
                                    href="{{route('player',['id'=>$item->id])}}"> {{$item->nickname}}</a></th>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->team->name}}</td>
                            <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$item->headshots}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
</x-app-layout>
