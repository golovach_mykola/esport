<x-app-layout>
    <x-slot name="header" >
        <div class="flex">
            <div class="inline">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight mt-2">
                    {{ __('Prizepics')  }}
                </h2>
            </div>
            <div style="margin-left: auto; margin-right: 0;" class="flex">
                <form method="get">
                    <select style="width: 120%" name="maps" onchange="window.location.href=this.options[this.selectedIndex].value" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 form-control me-2">
                        <option value="/prizepics?map=0" selected >Map &nbsp;</option>
                        @foreach($maps as $map)
                            <option value="/prizepics?map_id={{$map->id}}">{{$map->name}}</option>
                        @endforeach
                    </select>
                </form>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <form action="{{route('prizepics')}}" class="bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                    <button type="submit">
                        clear filter
                    </button>
                </form>


            </div>
        </div>
    </x-slot>

    <div class="grid grid-cols-1 sm:grid-cols-4 gap-5 mt-4 mx-auto max-w-7xl">
            @foreach($prs as $pr)
            <div class="">
                @include('layouts.card', compact('pr'))
            </div>
            @endforeach

    </div>
    <br>
    <div class="mt-1 text-gray-400 mx-auto  max-w-7xl">
        <x-last-update />
    </div>

</x-app-layout>
