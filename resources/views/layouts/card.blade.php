<div>
    <div class="flex justify-center">
        <div
            class="block max-w-sm rounded-lg bg-white text-center shadow-lg" style="min-width: 300px">
            <div
                class="border-b-2 border-gray-300 py-3 px-6 text-xl ">
                {{$pr->player->nickname}} - {{$pr->player->rating_2_0}}
                <br>
                @if($pr->hit_rate!=null)
                    <p style="color: grey">{{$pr->hit_rate}} %</p>
                @endif

                @foreach($SameNames as $n)
                    @if($n->nickname === $pr->player->nickname)
                        <form action="{{route('update-prizepics')}}" method="post" >
                            @method('PUT')
                            @csrf
                            <select name="player_id" style="width: 20%" onchange="this.form.submit()" class="bg-gray-50 border-2 border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 pb-1 form-control me-2 float-right">
                                <option> </option>
                                @foreach($SameNames as $name)
                                    @if(!strcasecmp($name->nickname, $pr->player->nickname))
                                    <option value="{{$name->id}}">{{$name->nickname}} - {{$name->team->name}}  </option>
                                    @endif
                                @endforeach
                            </select>
                            <input type="hidden" name="prize_id" value="{{$pr->id}}">
                        </form>
                        @break
                    @endif
                @endforeach
            </div>
            <div class="p-6">
                <h5
                    class="mb-2 text-lg font-medium leading-tight text-neutral-800">
                    {{$pr->team}} - {{$pr->position}}
                </h5>
                <p class="text-danger text-neutral-600 ">
                    VS
                </p>
                <p class="mb-4 text-base text-neutral-600 ">
                    {{$pr->opponent_team}} <br>
                </p>
                <p
                    class=" font-bold text-md inline-block rounded bg-primary bg-neutral-300 px-3 pt-2.5 pb-2  shadow"
                    data-te-ripple-init
                    data-te-ripple-color="light">
                    {{$pr->line_score}} || {{$pr->stat_type}}
                </p>
            </div>
            <div id="time" class="border-t-2 border-gray-300 py-3 px-6 ">
                Start in {{$pr->date_time->diffForHumans()}}
            </div>
        </div>
    </div>
</div>

