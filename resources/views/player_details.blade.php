<x-app-layout>
    <div class="container mx-auto max-w-4xl mt-3 pb-14">
        <a href="{{ route('dashboard') }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-left">Players</a>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <a class="bg-white text-xl ml-3 rounded-lg px-3 py-2 shadow-md border-2 border-orange-300 inline float-left">{{\Illuminate\Support\Facades\Session::get('message')}}</a>
        @endif

        <a href="{{ route('matches',['id'=>$player->id]) }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-right">Matches</a>
        <a href="{{ route('compare.player',array('pl1'=>$player->id)) }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-right mr-3">Compare</a>
        <a href="{{ route('edit.player',['id'=>$player->id]) }}" class="bg-white text-xl rounded-lg px-3 py-2 shadow-md border-2 border-indigo-100 inline float-right mr-3">Edit player</a>
    </div>
    <div class="container mx-auto max-w-4xl mt-2 columns-2 gap-2 ">
        <div class="container flex relative overflow-x-auto rounded-lg shadow-md bg-white" style="height: 384px"><img
                style="max-height: 384px" class="mx-auto my-auto rounded-lg border-0 border-white"
                src="{{$player->image}}" alt="{{$player->nickname}}"/></div>
        <div class="container relative overflow-x-auto rounded-lg shadow-md">
            <table class="w-full bg-white text-left text-sm text-gray-500">
                <tr>
                    <th class="bg-white px-6 py-6 text-2xl uppercase text-gray-700">Nickname</th>
                    <td class="bg-white px-6 py-6 text-2xl">{{$player->nickname}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-6 py-4 text-xl uppercase text-gray-700">Team</th>
                    <td class="bg-white px-6 py-4 text-xl">{{$player->team->name}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-6 py-4 text-xl uppercase text-gray-700">Rating 2.0</th>
                    <td class="bg-white px-6 py-4 text-xl uppercase">{{$player->rating_2_0}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-6 py-4 text-xl uppercase text-gray-700">DPR</th>
                    <td class="bg-white px-6 py-4 text-xl uppercase">{{round($player->total_deaths / $player->rounds_played,2)}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-6 py-4 text-xl uppercase text-gray-700">KPR</th>
                    <td class="bg-white px-6 py-4 text-xl uppercase">
                        {{round($player->total_kills / $player->rounds_played,2)}}
                    </td>
                </tr>
                <tr>
                    <th class="bg-white px-6 py-4 pb-5 text-xl uppercase text-gray-700">APR</th>
                    <td class="bg-white px-6 py-4 text-xl uppercase">{{$player->assists_per_round}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="container mx-auto max-w-4xl px-4 my-3">
        <h2 class="font-semibold text-2xl">
            Statistics
        </h2>
    </div>
    <div class="container mx-auto max-w-4xl  columns-2 gap-2">
        <div class="container relative overflow-x-auto rounded-lg shadow-md">
            <table class="w-full bg-white text-left text-sm text-gray-500">
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Total kills</th>
                    <td>{{$player->total_kills}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Headshot %</th>
                    <td>{{$player->headshots}}%</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Total deaths</th>
                    <td>{{$player->total_deaths}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">K/D Ratio</th>
                    <td>{{round($player->total_kills / $player->total_deaths,2)}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Damage / Round</th>
                    <td>{{$player->damage_per_round_av}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Grenade dmg / Round</th>
                    <td>{{$player->grenade_damage_round}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Maps played</th>
                    <td>{{$player->maps_played}}</td>
                </tr>
            </table>
        </div>
        <div class="container relative overflow-x-auto rounded-lg shadow-md">
            <table class="w-full bg-white text-left text-sm text-gray-500">
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">rounds played</th>
                    <td>{{$player->rounds_played}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Kills / Round</th>
                    <td>{{round($player->total_kills / $player->rounds_played,2)}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Assists / Round</th>
                    <td>{{$player->assists_per_round}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Death / Round</th>
                    <td>{{round($player->total_deaths/$player->rounds_played,2)}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Saved by teammate / Round</th>
                    <td>{{$player->saved_by_teammate_per_round}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Saved teamates / Round</th>
                    <td>{{$player->saved_teammates_per_round}}</td>
                </tr>
                <tr>
                    <th class="bg-white px-4 py-3 text-xs uppercase text-gray-700">Rating 2.0</th>
                    <td>{{$player->rating_2_0}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="container mx-auto max-w-4xl px-4 my-3">
        <h2 class="font-semibold text-2xl">
            Year Summary
        </h2>
    </div>
    <x-player-match-summary :player="$player->id"/>
    <div class="container mx-auto mt-2 max-w-4xl ">
        <div class="mt-1 text-gray-400 ml-1">
            <x-last-update/>
        </div>
    </div>
</x-app-layout>
