<div>
    <div class="container mx-auto max-w-4xl relative overflow-x-auto rounded-lg shadow-md">
        <table class="w-full bg-white text-left text-sm text-gray-500">
            <thead class="text-xs text-gray-700 uppercase bg-white">
                <tr>
                    <th scope="col" class="px-6 py-3 pt-4">Year</th>
                    <th scope="col" class="px-6 py-3">Match count</th>
                    <th scope="col" class="px-6 py-3">Average Rating 2.0</th>
                </tr>
            </thead>
            <tbody>
                @foreach($summary as $s)
                <tr>
                    <th scope="row" class=" px-6 py-3 font-medium text-gray-900 whitespace-nowrap bg-white">{{$s->year}}</th>
                    <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$s->mat}}</td>
                    <td class=" px-6 py-3 font-medium text-gray-700 whitespace-nowrap bg-white">{{$s->rat}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
