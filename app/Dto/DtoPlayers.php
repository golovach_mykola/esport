<?php

namespace App\Dto;

class DtoPlayers
{
    public function __construct(  public readonly string $nickname,
                                  public readonly string $link,
                                  public readonly string $team,
                                  public readonly int $maps_played,
                                  public readonly int $rounds_played,
                                  public readonly float $rating_1_0,
                                  public readonly int $kills_on_deaths_different,
                                  public readonly float $kills_on_deaths )
    {
    }
}
