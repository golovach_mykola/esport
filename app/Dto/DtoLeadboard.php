<?php

namespace App\Dto;

use Illuminate\Database\Eloquent\Collection;

class DtoLeadboard
{
    public function __construct(public readonly Collection  $rating_1_0,
                                public readonly Collection  $rating_2_0,
                                public readonly Collection  $kast,
                                public readonly Collection  $KD_diff,
                                public readonly Collection  $DamagePR,
                                public readonly Collection  $Kills,
                                public readonly Collection  $KillsPR,
                                public readonly Collection  $DeathPR,
                                public readonly Collection  $AsistPR,
                                public readonly Collection  $headshots)
    {
    }

}
