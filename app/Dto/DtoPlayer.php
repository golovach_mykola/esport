<?php

namespace App\Dto;

class DtoPlayer
{
    public function __construct(public readonly string $nickname,
                                public readonly string $team,
                                public readonly float  $rating_2_0,
                                public readonly float  $kast,
                                public readonly float  $impact,
                                public readonly float  $damage_per_round_av,
                                public readonly int    $total_kills,
                                public readonly float  $headshots,
                                public readonly int    $total_deaths,
                                public readonly float  $grenade_damage_round,
                                public readonly int    $maps_played,
                                public readonly int    $rounds_played,
                                public readonly float  $assists_per_round,
                                public readonly float  $saved_by_teammate_per_round,
                                public readonly float  $saved_teammates_per_round,
                                public readonly string $image
    )
    {

    }
}
