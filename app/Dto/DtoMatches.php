<?php

namespace App\Dto;

class DtoMatches
{
    public function __construct(public readonly string $nickname,
                                public readonly string $player_team,
                                public readonly string $opponent_team,
                                public readonly int $kills,
                                public readonly int $deaths,
                                public readonly float $rating,
                                public readonly string $map,
                                public readonly string $date)
    {
    }


}
