<?php

namespace App\Dto;

use Carbon\Carbon;

class DtoPrizepicks
{
    public function __construct(public readonly string $player,
                                public readonly string $team,
                                public readonly string $position,

                                public readonly string $stat_type,
                                public readonly Carbon $date,
                                public readonly float $line_score,
                                public readonly string $opponent_team,
                                public  readonly int $prizepics_id)
    {
    }
}
