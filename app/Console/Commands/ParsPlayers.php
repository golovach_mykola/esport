<?php

namespace App\Console\Commands;

use App\Http\Parsers\PlayersParser;
use App\Jobs\ParsePlayersJob;
use App\Repositories\PlayersParserRepository;
use Illuminate\Console\Command;

class ParsPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pars:players';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch((new ParsePlayersJob())->onQueue('pars_players'));
        return Command::SUCCESS;
    }
}
