<?php

namespace App\Console\Commands;

use App\Http\Parsers\PrizepicksParser;
use App\Repositories\PrizepicsRepository;
use Illuminate\Console\Command;

class ParsPrizepeicks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pars:prizepicks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(PrizepicksParser $parser, PrizepicsRepository $repository)
    {

        $arr=$parser->pars();
        $this->line("\n Parsing....\n");
        foreach ($arr as $value)
        {
            $repository->save($value);
            dump('saving');
        }
        $this->line("\n Prizepeicks parsed! \n");
        return Command::SUCCESS;

    }
}
