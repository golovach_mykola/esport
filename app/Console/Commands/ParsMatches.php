<?php

namespace App\Console\Commands;

use App\Http\Parsers\MatchParser;
use App\Http\Parsers\PlayersParser;
use App\Jobs\MatchParseJob;
use App\Models\Player;
use App\Repositories\MatchStaisticRepository;
use Illuminate\Console\Command;

class ParsMatches extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pars:matches';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parsing matches statistics';
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $players = Player::get();
        foreach ($players as $item) {
            dispatch((new MatchParseJob(str_replace('players', 'players/matches', $item->link)))->onQueue('player_matches'));
        }
        $this->line("\n The command was  successful! \n");
        return Command::SUCCESS;
    }
}
