<?php

namespace App\Console\Commands;

use App\Http\Parsers\PlayerParser;
use App\Jobs\PlayerParseJob;
use App\Models\Player;
use App\Repositories\PlayerRepository;
use Illuminate\Console\Command;

class ParsPlayer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pars:player';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command parse all details about players';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $players = Player::get();
        foreach ($players as $player){
            dispatch((new PlayerParseJob($player->link,$player->id))->onQueue('player_details'));
        }
        return Command::SUCCESS;
    }
}
