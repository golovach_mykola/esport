<?php

namespace App\Http\Parsers;

use App\Dto\DtoPrizepicks;
use App\Models\Match_statisticks;
use App\Models\Prizepics_statistics;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class PrizepicksParser
{
    public function pars()
    {
        $url = 'https://api.prizepicks.com/projections?league_id=124&per_page=250&single_stat=true';
        $api = Http::retry(1)->withHeaders(
            ['Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
                'Accept-Language' => 'uk-UA,uk;q=0.8,en-US;q=0.5,en;q=0.3',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.',])
            ->withOptions(['curl' => [CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_3]])
            ->get($url);
        $json = json_decode($api);

        $arr = array();
        foreach ($json->included as $inc){
            $prizepics_id = $inc->id;
        }
        foreach ($json->data as $data) {
            $opponent_team = $data->attributes->description;
            $line_score = $data->attributes->line_score;
            $stat_type = $data->attributes->stat_type;
            $id = $data->relationships->new_player->data->id;

            $date = Carbon::parse( $data->attributes->start_time);
            foreach ($json->included as $value) {
                if ($value->type === 'new_player') {
                    if ($id === $value->id) {
                        $player = $value->attributes->name;
                        $team = $value->attributes->team;
                        $position = $value->attributes->position;
                    }
                }
            }
            $arr[] = new DtoPrizepicks($player, $team, $position, $stat_type,$date, $line_score, $opponent_team, $prizepics_id);
        }
        return $arr;
    }
}
