<?php

namespace App\Http\Parsers;

use App\Dto\DtoMatches;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class MatchParser
{
    public function pars(string $url)
    {
        $html = Http::retry(5)->withHeaders([
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Accept-Language' => 'uk-UA,uk;q=0.8,en-US;q=0.5,en;q=0.3',
            'Connection' => 'keep-alive',
            'Cookie' => 'cf_zaraz_google-analytics_v4_8565=true; google-analytics_v4_8565__engagementStart=1678303114679; google-analytics_v4_8565__counter=117; google-analytics_v4_8565__session_counter=7; google-analytics_v4_8565__ga4=76c753aa-0730-495f-bd6a-59ade481bea3; google-analytics_v4_8565__let=1678303114679; MatchFilter={%22active%22:false%2C%22live%22:false%2C%22stars%22:1%2C%22lan%22:false%2C%22teams%22:[]}; _fbp=fb.1.1677580784314.1069494352; CookieConsent={stamp:%27qnhjESv8ADdCzVFWh50wWITwG10BTNnNMRY5yfDgLPGrGyxOiWGHcA==%27%2Cnecessary:true%2Cpreferences:true%2Cstatistics:true%2Cmarketing:true%2Cmethod:%27explicit%27%2Cver:1%2Cutc:1677580785562%2Cregion:%27ua%27}; __gads=ID=9196789ce3ffa056-2289874638dd00d4:T=1677580786:S=ALNI_MaNX9VuhHx6UIbNkX_l_xlhseaLzQ; __gpi=UID=00000bbd283534e0:T=1677580786:RT=1678301662:S=ALNI_MZLutqpX6cFNC6-_69xEGverJ65iA; google-analytics_v4_8565__engagementPaused=1677841557608; _ia__v4=%7B%22v%22%3A0%2C%22r%22%3A%22UA%22%2C%22sportsbook%22%3A%5B%5D%7D; _pbjs_userid_consent_data=6683316680106290; _lr_env_src_ats=false; cto_bundle=xi3RAV93JTJGVFUlMkJSUm5RRmlpZG5xeVBScHlncEhXb21PcnNUOHA0OEp1QU0ySHMzNjhuMHVzUVlTeWpHRCUyQnY1djBBMVYxSW9XV2QwRGdpME53MklVUVVLS0xVTUhXaFRnckI3ZFBWQW55RkhvRFZwZGF6aUNnakgyeG5BbGV4d0tnaEF6TA; cto_bidid=2CPb_F8lMkJNS1pvUnRTcmQ1MXloa09TR1c3QkxIVDZJZXRxWmZScXRjdU9ubGRUR0x5R09JRlhueXdBSFRJJTJCQTVYMlo0enVOZThza2lIb3hFaEh4cFNJSlh3bmclM0QlM0Q; __cf_bm=uFjLaStzPbjz0MWPE2LzzuxPun5OY8WKjP5t1btVj7k-1678302733-0-AUtjPqMkDEW/LJosCC/nkEmc/q9doU2IF5hGEwpF/fEZ9NfJ4grsMSgoSDOWHfzwgLwjhSwVLukOZnBANWPh7hXKGjBt+ix/SxM8lT1/GrnTI1CIsmkcAS64XHlgtCo6Fh0ZmFRfxav4aVN65sEviqCSDOnh/Y1ldKW4M/nXEQWn; google-analytics_v4_8565__ga4sid=2013599744; _lr_geo_location=UA; nightmode=auto; outbrain_cid_fetch=true',
            'Host' => 'www.hltv.org',
            'Sec-Fetch-Dest' => 'document',
            'Sec-Fetch-Mode' => 'navigate',
            'Sec-Fetch-Site' => 'same-origin',
            'Sec-Fetch-User' => '?1',
            'TE' => 'trailers',
            'Upgrade-Insecure-Requests' => '1',
            'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'])->get($url);
        $crawler = new Crawler($html->body());
        $table = $crawler->filter('table')->filter('.stats-table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        $player = $crawler->filter('.context-item-name')->text();
        $new_table = array();
        for ($i = 1; $i < count($table); $i++) {
            $b = 0;
            $b = explode(' - ', $table[$i][4]);
            $d = new DtoMatches(
                $player,
                rtrim(preg_replace('/\([^)]*\)|[()]/', '', $table[$i][1]), ' '),
                rtrim(preg_replace('/\([^)]*\)|[()]/', '', $table[$i][2]), ' '),
                (int)$b[0],
                (int)$b[1],
                (float)$table[$i][6],
                $table[$i][3],
                Carbon::rawCreateFromFormat('d/m/y', $table[$i][0])
            );
            $new_table[] = $d;
        }
        return $new_table;
    }
}
