<?php

namespace App\Http\Parsers;

use App\Dto\DtoPlayers;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class PlayersParser
{
    public function pars()
    {
        $html = Http::retry(10)->withHeaders(
            ['Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
                'Accept-Encoding' => 'gzip, deflate, br',
                'Accept-Language' => 'uk-UA,uk;q=0.8,en-US;q=0.5,en;q=0.3',
                'Connection' => 'keep-alive',
                'Cookie' => 'cf_zaraz_google-analytics_v4_8565=true; google-analytics_v4_8565__engagementStart=1678298684284; google-analytics_v4_8565__counter=6; google-analytics_v4_8565__session_counter=2; google-analytics_v4_8565__ga4=2a404878-0d00-4a80-b7cf-d6cfb2d06c9a; google-analytics_v4_8565__let=1678298684284; MatchFilter={%22active%22:false%2C%22live%22:false%2C%22stars%22:1%2C%22lan%22:false%2C%22teams%22:[]}; CookieConsent={stamp:%27Ff6rB/fniDwydh9BFYtfQUILLUAYpCTWureeuGGF6RiklPa0RtcM9Q==%27%2Cnecessary:true%2Cpreferences:true%2Cstatistics:true%2Cmarketing:true%2Cmethod:%27explicit%27%2Cver:1%2Cutc:1678270570838%2Cregion:%27ua%27}; _ia__v4=%7B%22v%22%3A0%2C%22r%22%3A%22UA%22%2C%22sportsbook%22%3A%5B%5D%7D; _fbp=fb.1.1678270575098.85165632; __gads=ID=ddadb0fcc00555a6-22a7a54a47dd00a2:T=1678270575:RT=1678270575:S=ALNI_MZvuupanz5rEaQRoXrFWNn0g03O0A; __gpi=UID=00000bc26ae5897d:T=1678270575:RT=1678297466:S=ALNI_MYOjFtz-lZDqnL1SXEAHzQevUuEDw; _lr_geo_location=UA; google-analytics_v4_8565__engagementPaused=1678270647648; __cf_bm=TFsFOoSWRyX.g_Rm.ndB8xFuEUcAGL2ckmPm5m.D1wk-1678298687-0-AWqHN4uKV5SJWq/V2ea8y0xUvYBV0e+LwVumkjwvJ6KnGioELcWOdVtRk8ws5srYG6IK9RYkhCdlkkUsgugQegRfWKForuQ+RmqMn/LGTx1oZlMOGhK0mNNpWxyqg3yQLb6cB+2NBUAoR/N9Isj8MEsG85JGrOQOtyPBV2az41x7; google-analytics_v4_8565__ga4sid=64703968; outbrain_cid_fetch=true',
                'Host' => 'www.hltv.org',
                'Sec-Fetch-Dest' => 'document',
                'Sec-Fetch-Mode' => 'navigate',
                'Sec-Fetch-Site' => 'none',
                'Sec-Fetch-User'=> '?1',
                'TE'=>'trailers',
                'Upgrade-Insecure-Requests' => '1',
                'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0',])
            ->get('https://www.hltv.org/stats/players');
        $crawler = new Crawler($html->body());
        $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                if ($td->text() === "") {
                    return trim($td->filter('a')->filter('img')->attr('alt'));
                } else
                    return trim($td->text());
            });
        });
        $links = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->filter('a')->each(function ($a, $i) {
                return trim($a->attr('href'));
            });
        });

        $new_table = array();
        for ($i = 1; $i < count($table); $i++) {
            $d = new DtoPlayers(
                $table[$i][0],
                'https://www.hltv.org' . $links[$i][0],
                $table[$i][1],
                $table[$i][2],
                $table[$i][3],
                $table[$i][6],
                $table[$i][4],
                $table[$i][5]);
            $new_table[] = $d;

        }
        return $new_table;
    }
}
