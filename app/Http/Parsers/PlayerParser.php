<?php

namespace App\Http\Parsers;

use App\Dto\DtoPlayer;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class PlayerParser
{
    public static function pars(string $url)
    {
        $html = Http::retry(50)->withHeaders([
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate, br',
            'Accept-Language' => 'en-US,en;q=0.5',
            'Connection' => 'keep-alive',
            'Cookie' => 'cf_zaraz_google-analytics_v4_8565=true; google-analytics_v4_8565__engagementStart=1678700742818; google-analytics_v4_8565__counter=8; google-analytics_v4_8565__session_counter=3; google-analytics_v4_8565__ga4=2a404878-0d00-4a80-b7cf-d6cfb2d06c9a; google-analytics_v4_8565__let=1678700742818; MatchFilter={%22active%22:false%2C%22live%22:false%2C%22stars%22:1%2C%22lan%22:false%2C%22teams%22:[]}; CookieConsent={stamp:%27Ff6rB/fniDwydh9BFYtfQUILLUAYpCTWureeuGGF6RiklPa0RtcM9Q==%27%2Cnecessary:true%2Cpreferences:true%2Cstatistics:true%2Cmarketing:true%2Cmethod:%27explicit%27%2Cver:1%2Cutc:1678270570838%2Cregion:%27ua%27}; _fbp=fb.1.1678270575098.85165632; __gads=ID=ddadb0fcc00555a6-22a7a54a47dd00a2:T=1678270575:RT=1678270575:S=ALNI_MZvuupanz5rEaQRoXrFWNn0g03O0A; __gpi=UID=00000bc26ae5897d:T=1678270575:RT=1678700745:S=ALNI_MYOjFtz-lZDqnL1SXEAHzQevUuEDw; google-analytics_v4_8565__engagementPaused=1678270647648; __cf_bm=fmDrPyiI4IZ39E1VBLW2_KWg1svltgXRLotuk2KwK34-1678700744-0-AYR5KUsr+B0gnXVLRKbwyJNridF0p/opKO/rUVrMLW7n9S2evFKbzWyARycd0dRAJgpcDaLORDiRE4suzLn9vOjtMSTA8c35aYLMHZHxTyWEAGYslmKsSDKThK8Qb2AJzGcyT5w98e5PW9Gq8Rdb7a69tyhXsKBHroaUP02UhWEV; google-analytics_v4_8565__ga4sid=1463139531; outbrain_cid_fetch=true; _ia__v4=%7B%22v%22%3A0%2C%22r%22%3A%22UA%22%2C%22sportsbook%22%3A%5B%5D%7D; _lr_geo_location=UA',
            'Host' => 'www.hltv.org',
            'Sec-Fetch-Dest' => 'document',
            'Sec-Fetch-Mode' => 'navigate',
            'Sec-Fetch-Site' => 'cross-site',
            'Sec-Fetch-User'=> '?1',
            'TE'=>'trailers',
            'Upgrade-Insecure-Requests' => '1',
            'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0'
        ])->get($url);
        $crawler = new Crawler();
        $crawler->addHTMLContent($html->body());
        $nickname = trim($crawler->filter('.summaryNickname')->text());
        $team = trim($crawler->filter('.SummaryTeamname')->text());
        $smoll_stats = $crawler->filter('.summaryBreakdownContainer')->each(function ($tr) {
            return $tr->filter('.summaryStatBreakdown')->each(function ($ae) {
                return trim($ae->filter('.summaryStatBreakdownDataValue')->text());
            });
        });

        $full_stats = $crawler->filter('.statistics')->each(function ($tr) {
            return $tr->filter('.stats-row')->each(function ($td) {
                return $td->filter('span')->last()->each(function ($ts) {
                    return trim($ts->text());
                });
            });
        });

        $name = preg_replace('/ /',' \'' . $nickname . '\' ', trim($crawler->filter('.summaryRealname')->text()),1);

        $image = trim($crawler->selectImage($name)->attr('src'));
        if(substr($image,0,1)=='/') $image='https://www.hltv.org'.$image;

        $table = new DtoPlayer(
            (string)$nickname,
            (string)$team,
            (float)$smoll_stats[0][0],
            (float)$smoll_stats[0][2],
            (float)$smoll_stats[0][3],
            (float)$full_stats[0][4][0],
            (int)$full_stats[0][0][0],
            (float)$full_stats[0][1][0],
            (int)$full_stats[0][2][0],
            (float)$full_stats[0][5][0],
            (int)$full_stats[0][6][0],
            (int)$full_stats[0][7][0],
            (float)$full_stats[0][9][0],
            (float)$full_stats[0][11][0],
            (float)$full_stats[0][12][0],
            (string)$image);

        return $table;

    }
}
