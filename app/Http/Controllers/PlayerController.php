<?php

namespace App\Http\Controllers;

use App\Http\Parsers\PlayerParser;
use App\Http\Requests\PlayerEditRequest;
use App\Models\Match_statisticks;
use App\Models\Player;
use App\Models\Team;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function show($id){
        $player = Player::find($id);

        return view('player_details',['player'=>$player]);
    }

    public function editpage($id){
        $player = Player::find($id);
        $teams = Team::get();

        return view('player_edit',['player'=>$player,'teams'=>$teams]);
    }
    public function match_summary($id){
        $matches = Match_statisticks::where('player_id',$id)->get();
        dd($matches);
    }
    public function update($id, PlayerEditRequest $request){
        $player = Player::find($id);

        $player->nickname=$request->get('nickname');
        $player->link=$request->get('link');
        $player->team_id=$request->get('team_id');
        $player->maps_played=$request->get('maps_played');
        $player->rounds_played=$request->get('rounds_played');
        $player->rating_1_0=$request->get('rating_1_0');
        $player->kills_on_deaths_different=$request->get('kills_on_deaths_different');
        $player->kills_on_deaths=$request->get('kills_on_deaths');
        $player->rating_2_0=$request->get('rating_2_0');
        $player->kast=$request->get('kast');
        $player->impact=$request->get('impact');
        $player->damage_per_round_av=$request->get('damage_per_round_av');
        $player->total_kills=$request->get('total_kills');
        $player->headshots=$request->get('headshots');
        $player->total_deaths=$request->get('total_deaths');
        $player->grenade_damage_round=$request->get('grenade_damage_round');
        $player->assists_per_round=$request->get('assists_per_round');
        $player->saved_by_teammate_per_round=$request->get('saved_by_teammate_per_round');
        $player->saved_teammates_per_round=$request->get('saved_teammates_per_round');
        $player->image=$request->get('image');

        $player->save();
        return redirect(route('player',['id'=>$id]))->with('message', 'Player was updated');
    }
}
