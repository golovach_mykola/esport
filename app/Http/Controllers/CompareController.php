<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\Team;
use App\Repositories\PlayerRepository;
use Illuminate\Http\Request;

class CompareController extends Controller
{
    public function show(Request $request,PlayerRepository $playerRepository){
        $pl1=$request->get('pl1');
        $pl2=$request->get('pl2');
        $player1=$playerRepository->getPlayer($pl1);
        $player2=$playerRepository->getPlayer($pl2);

        $players=$playerRepository->getPlayers();
        $teams=$playerRepository->getTeams();
        return view('compare',['player1'=>$player1,'player2'=>$player2,'players'=>$players, 'teams'=>$teams])->with('pl1',$pl1)->with('pl2',$pl2);
    }
}
