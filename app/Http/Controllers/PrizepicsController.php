<?php
namespace App\Http\Controllers;
use App\Http\Parsers\MatchParser;
use App\Http\Parsers\PrizepicksParser;
use App\Models\Map;
use App\Models\Match_statisticks;
use App\Models\Player;
use App\Models\Prizepics_statistics;
use App\Repositories\MatchStaisticRepository;
use App\Repositories\PrizepicsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PrizepicsController extends Controller
{
    public function show(PrizepicsRepository $repository, Request $request)
    {
        $map_id = $request->get('map_id');
        $prs = $repository->get_prizepics($map_id);
        foreach ($prs as $key => $val) {
            $val['date_time'] = new Carbon($val['date_time']);
        }
        $maps = Map::query()->orderBy('name')->get();
        $SameNames = $repository->get_same_names();
        return (view('prizepics', compact('prs', 'maps', 'SameNames')));
    }
    public static function pr_change( Request $request){
        $player = Player::query()->where('id', '=', $request->player_id)->first();
        $praize = Prizepics_statistics::query()->where('id', '=', $request->prize_id)->first();
        $player1 = Player::query()->where('id', '=', $request->player_id)->first();
        $player1->prizepics_id = null;
        $player1->save();
        $praize->player_id = $player->id;
        $praize->save();
        $player->prizepics_id = $praize->prizepics_id;
        $player->save();
        return redirect()->back();

    }
    public function history(PrizepicsRepository $repository, Request $request)
    {
        $sort=$request->get('sortBy');
        $sortType=$request->get('sortType','Asc');
        $team=$request->get('team');
        $search=$request->get('search_input');

        $players=$repository->get_history($search, $sort, $sortType,$team);
        $teams=$repository->get_teams();
        return view('history',compact('players','teams', 'sortType','search','team'));
    }
}
