<?php

namespace App\Http\Controllers;

use App\Dto\DtoMatches;
use App\Jobs\MatchParseJob;
use App\Models\Map;
use App\Models\Match_statisticks;
use App\Models\Player;
use App\Repositories\MatchStaisticRepository;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class MatchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function show_matches(int $id, Request $request, MatchStaisticRepository $repository)
    {
        $map_id = $request->get('map_id');
        $p_name = Player::find($id);
        $sort = $request->get('sortBy');
        $sortType = $request->get('sortType', 'Asc');
        $search = $request->get('search_input');
        $matches = $repository->get_matches($search, $sort, $sortType, $id, $map_id);
        $maps = Map::query()->orderBy('name')->get();
        return view('match-statistics', compact('matches', 'sortType', 'p_name', 'id', 'search', 'map_id', 'maps'));
    }
}
