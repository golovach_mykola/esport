<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Repositories\PlayersParserRepository;
use Illuminate\Http\Request;

class PlayersController extends Controller
{
    public function open_players(Request $request, PlayersParserRepository $repository)
    {

        $sort = $request->get('sortBy');
        $sortType = $request->get('sortType', 'Asc');
        $team_id = $request->get('team_id');
        $search = $request->get('search_input');

        $players = $repository->get_players($search, $sort, $sortType, $team_id);

        $teams = Team::orderBy('name')->get();
        return view('players', compact('players', 'teams', 'sortType', 'search', 'team_id'));
    }

    public function leaderboard(PlayersParserRepository $player_repo)
    {
        $players=$player_repo->leaderboards();
        return view('leaderboard', compact('players'));
    }
}
