<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlayerEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nickname'=>'required|max:255',
            'link'=>'required|url',
            'team_id'=>'required|integer',
            'maps_played'=>'required|integer',
            'rounds_played'=>'required|integer',
            'rating_1_0'=>'required|numeric',
            'kills_on_deaths_different'=>'required|integer',
            'kills_on_deaths'=>'required|numeric',
            'rating_2_0'=>'required|numeric',
            'kast'=>'required|numeric',
            'impact'=>'required|numeric',
            'damage_per_round_av'=>'required|numeric',
            'total_kills'=>'required|integer',
            'headshots'=>'required|numeric',
            'total_deaths'=>'required|integer',
            'grenade_damage_round'=>'required|numeric',
            'assists_per_round'=>'required|numeric',
            'saved_by_teammate_per_round'=>'required|numeric',
            'saved_teammates_per_round'=>'required|numeric',
            'image'=>'required|url'
        ];
    }
}
