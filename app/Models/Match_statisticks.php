<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match_statisticks extends Model
{
    protected $table = 'match_statistics';

    protected $fillable = ['player_id', 'players_team_id', 'opponent_team_id','kills','deaths', 'rating', 'map_id','date_time' ];
    use HasFactory;


    public function p_name(){
        return $this->belongsTo(Player::class,'player_id');
    }
    public function team(){
        return $this->belongsTo(Team::class,'players_team_id');
    }
    public function opponent_team(){
        return $this->belongsTo(Team::class,'opponent_team_id');
    }
    public function map(){
        return $this->belongsTo(Map::class,'map_id');
    }


}
