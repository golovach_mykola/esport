<?php

namespace App\View\Components;

use App\Models\Match_statisticks;
use App\Models\Prizepics_statistics;
use Carbon\Carbon;
use Illuminate\View\Component;

class LastUpdate extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $hltv = Match_statisticks::query()->latest('updated_at')->first('updated_at');
        if ($hltv){
            $last_update_hltv = new Carbon($hltv->updated_at);
            $last_update_hltv = $last_update_hltv->diffForHumans();
        }
        else{
            $last_update_hltv = 'unknown';
        }

        $prizepics = Prizepics_statistics::query()->latest('updated_at')->first('updated_at');
        if ($prizepics){
            $last_update_prizepics = new Carbon($prizepics->updated_at);
            $last_update_prizepics = $last_update_prizepics->diffForHumans();
        }
        else{
            $last_update_prizepics = 'unknown';
        }
        return view('components.last-update', compact('last_update_hltv', 'last_update_prizepics'));
    }
}
