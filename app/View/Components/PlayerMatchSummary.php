<?php

namespace App\View\Components;

use App\Models\Match_statisticks;
use Illuminate\View\Component;

class PlayerMatchSummary extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $player_id;
    public function __construct($player)
    {
        $this->player_id=$player;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $matches = Match_statisticks::where('player_id',$this->player_id)->get();

        $summary = Match_statisticks::query()
            ->where('player_id','=',$this->player_id)
            ->selectRaw('YEAR(date_time) as year,COUNT(*) as mat,ROUND(AVG(rating),2) as rat')
            ->groupByRaw('year')
            ->get();

        return view('components.player-match-summary',['summary'=>$summary]);
    }
}
