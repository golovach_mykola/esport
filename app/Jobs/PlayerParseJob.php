<?php

namespace App\Jobs;

use App\Http\Parsers\PlayerParser;
use App\Models\Player;
use App\Repositories\PlayerRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PlayerParseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private string $p_link,private int $p_id)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PlayerParser $parser,PlayerRepository $repo)
    {
        $info=$parser->pars($this->p_link);
        $repo->update($info,$this->p_id);
        sleep(1);
    }
}
