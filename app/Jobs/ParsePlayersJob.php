<?php

namespace App\Jobs;

use App\Http\Parsers\PlayersParser;
use App\Repositories\PlayersParserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ParsePlayersJob implements ShouldQueue{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PlayersParser $parser,PlayersParserRepository $repository)
    {
        $table=$parser->pars();
        foreach ($table as $value)
        {
            $repository->save($value);
        }
    }
}
