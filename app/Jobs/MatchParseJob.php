<?php

namespace App\Jobs;

use App\Console\Commands\ParsMatches;
use App\Http\Parsers\MatchParser;
use App\Http\Parsers\PlayersParser;
use App\Repositories\MatchStaisticRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MatchParseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private string $link)
    {
        //
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MatchParser $parser , MatchStaisticRepository $repository)
    {
        $MatchHtml = $parser->pars($this->link);
        foreach ($MatchHtml as $item) {
            $repository->save($item);
        }
        sleep(1);
    }
}
