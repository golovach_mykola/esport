<?php

namespace App\Repositories;

use App\Dto\DtoPlayer;
use App\Models\Player;
use App\Models\Team;

class PlayerRepository
{
    public function update($array,$p_id){
        $team = Team::query()->firstOrCreate(['name' => $array->team]);
        Player::query()->updateOrCreate([
            'id'=>$p_id
        ], [
            'team_id'=>$team->id,
            'rating_2_0'=>$array->rating_2_0,
            'kast'=>$array->kast,
            'impact'=>$array->impact,
            'damage_per_round_av'=>$array->damage_per_round_av,
            'total_kills'=>$array->total_kills,
            'headshots'=>$array->headshots,
            'total_deaths'=>$array->total_deaths,
            'grenade_damage_round'=>$array->grenade_damage_round,
            'maps_played'=>$array->maps_played,
            'rounds_played'=>$array->rounds_played,
            'assists_per_round'=>$array->assists_per_round,
            'saved_by_teammate_per_round'=>$array->saved_by_teammate_per_round,
            'saved_teammates_per_round'=>$array->saved_teammates_per_round,
            'image'=>$array->image
        ]);
    }
    public function getPlayer($id){
        return Player::where('id',$id)->first();
    }
    public function getPlayers(){
        return Player::get();
    }
    public function getTeams(){
        return Team::get();
    }
}
