<?php

namespace App\Repositories;

use App\Models\Map;
use App\Models\Match_statisticks;
use App\Models\Player;
use App\Models\Team;
use Illuminate\Database\Eloquent\Builder;

class MatchStaisticRepository
{
    public function save($array)
    {
        $map = Map::query()->firstOrCreate(['name' => $array->map]);
        $player_id = Player::query()->firstOrCreate(['nickname' => $array->nickname]);
        $opponent_team_id = Team::query()->firstOrCreate(['name' => $array->opponent_team]);
        $players_team_id = Team::query()->firstOrCreate(['name' => $array->player_team]);
        Match_statisticks::query()->updateOrCreate([
            'player_id' => $player_id->id,
            'players_team_id' => $players_team_id->id,
            'opponent_team_id' => $opponent_team_id->id,
            'date_time' => $array->date,
            'map_id' => $map->id], [
            'player_id' => $player_id->id,
            'players_team_id' => $players_team_id->id,
            'opponent_team_id' => $opponent_team_id->id,
            'date_time  ' => $array->date,
            'map_id' => $map->id,
            'kills' => $array->kills,
            'deaths' => $array->deaths,
            'rating' => $array->rating,
        ]);
    }

    public function get_matches($search, $sort, $sortType,$id,$map_id)
    {
        $searchq= '%'. $search .'%';
        $matches = Match_statisticks::query()->where('player_id', '=', $id)
            ->when($map_id, function (Builder $builder) use ($map_id){
                $builder->where('map_id', '=', $map_id);
            })
            ->when($searchq,function(Builder $builder) use ($searchq){
                $builder->where('date_time','LIKE', $searchq);
            })
            ->when($sort, function (Builder $builder) use ($sort, $sortType) {
                $builder->orderBy($sort, $sortType);
            })->paginate(20);
            $matches->appends(['search_input'=>$search, 'sortBy'=>$sort,'sortType'=>$sortType, 'map_id'=>$map_id]);
        return $matches;
    }
}
