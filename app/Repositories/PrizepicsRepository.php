<?php

namespace App\Repositories;

use App\Http\Parsers\MatchParser;
use App\Models\Match_statisticks;
use App\Models\Player;
use App\Models\Prizepics_statistics;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Date;
use function PHPUnit\Framework\matches;

class PrizepicsRepository
{
    public function get_hit_rate($id,$stat_type,$line_score)
    {

        $matches=Match_statisticks::query()->select('kills')->where('player_id','=',$id)->limit(10)->get();
        if($stat_type=='MAP 3 Kills' or $stat_type=='MAPS 1-2 Kills')
        {
            $count=0;

            foreach ($matches as $value)
            {
                if($line_score>$value->kills)
                    $count++;
            }
        }
        else
            return null;
        return $count*10;
    }
    public function save($table)
    {
        $player = Player::query()->where('nickname', '=', $table->player)->where('prizepics_id', '=', $table->prizepics_id)->first();
        if ($player == null) {
            $player = Player::query()->where('nickname', '=' ,$table->player)->first();
            if ($player==null){
                return 0;
            }
            else{
                $player->prizepics_id = $table->prizepics_id;
                $player->save();
            }
        }
        Prizepics_statistics::query()->updateOrCreate([
            'player_id' => $player->id,
            'opponent_team' => $table->opponent_team,
            'stat_type' => $table->stat_type,
        ], [
            'player_id' => $player->id,
            'team' => $table->team,
            'position' => $table->position,
            'date_time' => $table->date,
            'stat_type' => $table->stat_type,
            'line_score' => $table->line_score,
            'opponent_team' => $table->opponent_team,
            'prizepics_id' => $table->prizepics_id,
            'hit_rate'=>$this->get_hit_rate($player->id,$table->stat_type,$table->line_score),

        ]);
    }
    public function get_prizepics($map_id)
    {
        $prizepics = Prizepics_statistics::query()
            ->when($map_id, function (Builder $builder) use ($map_id) {
                $builder->join('match_statistics', 'prizepics_statistics.player_id', '=', 'match_statistics.player_id')
                    ->where('match_statistics.map_id', '=', $map_id);
            })
            ->whereRaw('prizepics_statistics.stat_type LIKE \'MAPS 1-2%\'')
            ->where('prizepics_statistics.date_time', '>', Date::now() )
            ->groupBy('prizepics_statistics.id')
            ->select('prizepics_statistics.*')
            ->get();
        $prizepics->append(['map_id' => $map_id]);
        return $prizepics;
    }

    public function get_same_names(){
        $SameName = Player::query()
            ->select('nickname')
            ->groupBy('nickname')
            ->havingRaw('COUNT(*) > 1')
            ->get()
            ->pluck('nickname');
        $SameNames = Player::query()
            ->whereIn('nickname', $SameName)
            ->orderBy('nickname')
            ->get();
        return $SameNames;
    }

    public function get_history($search, $sort, $sortType, $team)
    {
        $players =Prizepics_statistics::query()
            ->when($team, function (Builder $builder) use ($team){
            $builder->where('team','=', $team);
        })
            ->when($search,function(Builder $builder) use ($search, $team){

                $builder->where('nickname','LIKE', $search.'%');
            })
            ->when($sort, function (Builder $builder) use ($sort, $sortType) {
                $builder->orderBy($sort, $sortType);
            })->select('players.nickname','prizepics_statistics.team', 'prizepics_statistics.date_time','players.total_kills','players.headshots', 'players.rating_1_0')
            ->join('players','players.id','=','prizepics_statistics.player_id')
            ->paginate(11);
        return $players;
    }
    public function get_teams()
    {
        $teams = Prizepics_statistics::query()->select('team')->groupBy('team')->orderBy('team')->get();
        return $teams;
    }



}
