<?php

namespace App\Repositories;

use App\Dto\DtoLeadboard;
use App\Models\Player;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class PlayersParserRepository
{
    public function save($table)
    {
        $team = Team::query()->firstOrCreate(['name' => $table->team]);
        Player::query()->updateOrCreate([
            'nickname' => $table->nickname,
            'link' => $table->link,
        ], [
            'nickname' => $table->nickname,
            'link' => $table->link,
            'maps_played' => $table->maps_played,
            'team_id' => $team->id,
            'rounds_played' => $table->rounds_played,
            'kills_on_deaths_different' => $table->kills_on_deaths_different,
            'kills_on_deaths' => $table->kills_on_deaths,
            'rating_1_0' => $table->rating_1_0,
        ]);
    }

    public function get_players($search, $sort, $sortType, $team_id)
    {
        $players = Player::query()
            ->when($team_id, function (Builder $builder) use ($team_id) {
                $builder->where('team_id', '=', $team_id);
            })
            ->when($search, function (Builder $builder) use ($search, $team_id) {

                $builder->where('nickname', 'LIKE', '%' . $search . '%');
            })
            ->when($sort, function (Builder $builder) use ($sort, $sortType) {
                $builder->orderBy($sort, $sortType);
            })->paginate(11);

        $players->appends(['sortBy' => $sort, 'sortType' => $sortType, 'team_id' => $team_id, 'search_input' => $search]);

        return $players;
    }

    public function leaderboards()
    {
        $rating1_0 = Player::query()->orderBy('rating_1_0', 'desc')->limit(5)->get();
        $rating2_0 = Player::query()->orderBy('rating_1_0', 'desc')->limit(5)->get();
        $KD_diff = Player::query()->orderBy('kills_on_deaths_different', 'desc')->limit(5)->get();
        $DamagePR = Player::query()->orderBy('damage_per_round_av', 'desc')->limit(5)->get();
        $Kills = Player::query()->orderBy('total_kills', 'desc')->limit(5)->get();
        $KillsPR = Player::query()->selectRaw('ROUND(total_kills / rounds_played, 2) as KPR, nickname, team_id, id')->orderBy('KPR', 'desc')->limit(5)->get();
        $DeathPR = Player::query()->selectRaw('ROUND(total_deaths / rounds_played, 2) as DeathPR, nickname, team_id, id')->orderBy('DeathPR')->limit(5)->get();
        $AsistPR = Player::query()->orderBy('assists_per_round', 'desc')->limit(5)->get();
        $KAST = Player::query()->orderBy('kast', 'desc')->limit(5)->get();
        $headshots = Player::query()->orderBy('headshots', 'desc')->limit(5)->get();
        $players= new DtoLeadboard($rating1_0, $rating2_0, $KAST, $KD_diff, $DamagePR, $Kills, $KillsPR, $DeathPR, $AsistPR, $headshots);
        return $players;
    }
}
