<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard','\App\Http\Controllers\PlayersController@open_players')->name('dashboard');
    Route::get('/player/{id}','App\Http\Controllers\PlayerController@show')->name('player');
    Route::get('/player/{id}/matches', '\App\Http\Controllers\MatchController@show_matches')->name('matches');
    Route::get('/prizepics', '\App\Http\Controllers\PrizepicsController@show')->name('prizepics');
    Route::get('/history', '\App\Http\Controllers\PrizepicsController@history')->name('history');
    Route::get('/player/{id}/edit','App\Http\Controllers\PlayerController@editpage')->name('edit.player');
    Route::put('/player/{id}/update','App\Http\Controllers\PlayerController@update')->name('update.player');
    Route::put('/update/prizepics', '\App\Http\Controllers\PrizepicsController@pr_change')->name('update-prizepics');
    Route::get('/leaderboard','App\Http\Controllers\PlayersController@leaderboard')->name('leaderboard');
    Route::get('/compare', '\App\Http\Controllers\CompareController@show')->name('compare.player');
});
