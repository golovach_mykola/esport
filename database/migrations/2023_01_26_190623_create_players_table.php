<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('nickname');
            $table->string('link');
            $table->unsignedBigInteger('team_id');
            $table->unsignedInteger('maps_played');
            $table->unsignedInteger('rounds_played');
            $table->unsignedFloat('rating_1_0');
            $table->integer('kills_on_deaths_different');
            $table->float('kills_on_deaths');

            $table->unsignedFloat('rating_2_0')->nullable();
            $table->unsignedFloat('kast')->nullable();
            $table->unsignedFloat('impact')->nullable();
            $table->unsignedFloat('damage_per_round_av')->nullable();
            $table->unsignedInteger('total_kills')->nullable();
            $table->unsignedFloat('headshots')->nullable();
            $table->unsignedInteger('total_deaths')->nullable();
            $table->unsignedFloat('grenade_damage_round')->nullable();
            $table->unsignedFloat('assists_per_round')->nullable();
            $table->unsignedFloat('saved_by_teammate_per_round')->nullable();
            $table->unsignedFloat('saved_teammates_per_round')->nullable();
            $table->string('image')->nullable();
            $table->unsignedBigInteger('prizepics_id')->nullable();
            $table->timestamps();

            $table->foreign('team_id','player_team_fk')->on('teams')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
};
