<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_statistics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('players_team_id');
            $table->unsignedBigInteger('opponent_team_id');
            $table->unsignedInteger('kills');
            $table->unsignedInteger('deaths');
            $table->unsignedFloat('rating');
            $table->unsignedBigInteger('map_id');
            $table->date('date_time');
            $table->timestamps();

            $table->foreign('player_id','match_statistic_player_fk')->on('players')->references('id');

            $table->foreign('map_id','match_statistic_map_fk')->on('maps')->references('id');

            $table->foreign('players_team_id','match_statistic_team_1_fk')->on('teams')->references('id');
            $table->foreign('opponent_team_id','match_statistic_team_2_fk')->on('teams')->references('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_statisticks');
    }
};
