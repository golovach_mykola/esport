<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizepics_statistics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('player_id');
            $table->string('team');
            $table->string('position');
            $table->dateTime('date_time');
            $table->string('stat_type');
            $table->unsignedBigInteger('prizepics_id')->nullable();
            $table->unsignedFloat('line_score');
            $table->string('opponent_team');
            $table->timestamps();

            $table->foreign('player_id','prizepics_statistics_player_fk')->on('players')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizepics_statistics');
    }
};
